﻿namespace csharp2_lesson7
{
    partial class BodyFatCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.calculatorPage = new System.Windows.Forms.TabPage();
            this.chartPage = new System.Windows.Forms.TabPage();
            this.weightLabel = new System.Windows.Forms.Label();
            this.waistLabel = new System.Windows.Forms.Label();
            this.wristLabel = new System.Windows.Forms.Label();
            this.hipsLabel = new System.Windows.Forms.Label();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.waistTextBox = new System.Windows.Forms.TextBox();
            this.wristTextBox = new System.Windows.Forms.TextBox();
            this.hipsTextBox = new System.Windows.Forms.TextBox();
            this.forearmLabel = new System.Windows.Forms.Label();
            this.forearmTextBox = new System.Windows.Forms.TextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.femaleRadioButton = new System.Windows.Forms.RadioButton();
            this.maleRadioButton = new System.Windows.Forms.RadioButton();
            this.calculateButton = new System.Windows.Forms.Button();
            this.bodyFatLabel = new System.Windows.Forms.Label();
            this.weightUnitLabel = new System.Windows.Forms.Label();
            this.waistUnitLabel = new System.Windows.Forms.Label();
            this.wristUnitLabel = new System.Windows.Forms.Label();
            this.hipsUnitLabel = new System.Windows.Forms.Label();
            this.forearmUnitLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.calculatorPage.SuspendLayout();
            this.chartPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.calculatorPage);
            this.tabControl1.Controls.Add(this.chartPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(449, 366);
            this.tabControl1.TabIndex = 0;
            // 
            // calculatorPage
            // 
            this.calculatorPage.Controls.Add(this.label7);
            this.calculatorPage.Controls.Add(this.forearmUnitLabel);
            this.calculatorPage.Controls.Add(this.hipsUnitLabel);
            this.calculatorPage.Controls.Add(this.wristUnitLabel);
            this.calculatorPage.Controls.Add(this.waistUnitLabel);
            this.calculatorPage.Controls.Add(this.weightUnitLabel);
            this.calculatorPage.Controls.Add(this.bodyFatLabel);
            this.calculatorPage.Controls.Add(this.calculateButton);
            this.calculatorPage.Controls.Add(this.maleRadioButton);
            this.calculatorPage.Controls.Add(this.femaleRadioButton);
            this.calculatorPage.Controls.Add(this.forearmTextBox);
            this.calculatorPage.Controls.Add(this.forearmLabel);
            this.calculatorPage.Controls.Add(this.hipsTextBox);
            this.calculatorPage.Controls.Add(this.wristTextBox);
            this.calculatorPage.Controls.Add(this.waistTextBox);
            this.calculatorPage.Controls.Add(this.weightTextBox);
            this.calculatorPage.Controls.Add(this.hipsLabel);
            this.calculatorPage.Controls.Add(this.wristLabel);
            this.calculatorPage.Controls.Add(this.waistLabel);
            this.calculatorPage.Controls.Add(this.weightLabel);
            this.calculatorPage.Location = new System.Drawing.Point(4, 22);
            this.calculatorPage.Name = "calculatorPage";
            this.calculatorPage.Padding = new System.Windows.Forms.Padding(3);
            this.calculatorPage.Size = new System.Drawing.Size(441, 340);
            this.calculatorPage.TabIndex = 0;
            this.calculatorPage.Text = "Calculator";
            this.calculatorPage.UseVisualStyleBackColor = true;
            // 
            // chartPage
            // 
            this.chartPage.Controls.Add(this.label12);
            this.chartPage.Controls.Add(this.label11);
            this.chartPage.Controls.Add(this.label10);
            this.chartPage.Controls.Add(this.label9);
            this.chartPage.Controls.Add(this.label8);
            this.chartPage.Location = new System.Drawing.Point(4, 22);
            this.chartPage.Name = "chartPage";
            this.chartPage.Padding = new System.Windows.Forms.Padding(3);
            this.chartPage.Size = new System.Drawing.Size(441, 340);
            this.chartPage.TabIndex = 1;
            this.chartPage.Text = "Chart";
            this.chartPage.UseVisualStyleBackColor = true;
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(34, 16);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(41, 13);
            this.weightLabel.TabIndex = 0;
            this.weightLabel.Text = "Weight";
            // 
            // waistLabel
            // 
            this.waistLabel.AutoSize = true;
            this.waistLabel.Location = new System.Drawing.Point(34, 53);
            this.waistLabel.Name = "waistLabel";
            this.waistLabel.Size = new System.Drawing.Size(34, 13);
            this.waistLabel.TabIndex = 1;
            this.waistLabel.Text = "Waist";
            // 
            // wristLabel
            // 
            this.wristLabel.AutoSize = true;
            this.wristLabel.Location = new System.Drawing.Point(34, 89);
            this.wristLabel.Name = "wristLabel";
            this.wristLabel.Size = new System.Drawing.Size(31, 13);
            this.wristLabel.TabIndex = 2;
            this.wristLabel.Text = "Wrist";
            // 
            // hipsLabel
            // 
            this.hipsLabel.AutoSize = true;
            this.hipsLabel.Location = new System.Drawing.Point(34, 122);
            this.hipsLabel.Name = "hipsLabel";
            this.hipsLabel.Size = new System.Drawing.Size(28, 13);
            this.hipsLabel.TabIndex = 3;
            this.hipsLabel.Text = "Hips";
            // 
            // weightTextBox
            // 
            this.weightTextBox.Location = new System.Drawing.Point(166, 16);
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(100, 20);
            this.weightTextBox.TabIndex = 4;
            // 
            // waistTextBox
            // 
            this.waistTextBox.Location = new System.Drawing.Point(166, 46);
            this.waistTextBox.Name = "waistTextBox";
            this.waistTextBox.Size = new System.Drawing.Size(100, 20);
            this.waistTextBox.TabIndex = 5;
            // 
            // wristTextBox
            // 
            this.wristTextBox.Location = new System.Drawing.Point(163, 82);
            this.wristTextBox.Name = "wristTextBox";
            this.wristTextBox.Size = new System.Drawing.Size(100, 20);
            this.wristTextBox.TabIndex = 6;
            // 
            // hipsTextBox
            // 
            this.hipsTextBox.Location = new System.Drawing.Point(163, 122);
            this.hipsTextBox.Name = "hipsTextBox";
            this.hipsTextBox.Size = new System.Drawing.Size(100, 20);
            this.hipsTextBox.TabIndex = 7;
            // 
            // forearmLabel
            // 
            this.forearmLabel.AutoSize = true;
            this.forearmLabel.Location = new System.Drawing.Point(34, 152);
            this.forearmLabel.Name = "forearmLabel";
            this.forearmLabel.Size = new System.Drawing.Size(45, 13);
            this.forearmLabel.TabIndex = 8;
            this.forearmLabel.Text = "Forearm";
            // 
            // forearmTextBox
            // 
            this.forearmTextBox.Location = new System.Drawing.Point(166, 152);
            this.forearmTextBox.Name = "forearmTextBox";
            this.forearmTextBox.Size = new System.Drawing.Size(100, 20);
            this.forearmTextBox.TabIndex = 9;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(357, 395);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // femaleRadioButton
            // 
            this.femaleRadioButton.AutoSize = true;
            this.femaleRadioButton.Location = new System.Drawing.Point(37, 188);
            this.femaleRadioButton.Name = "femaleRadioButton";
            this.femaleRadioButton.Size = new System.Drawing.Size(59, 17);
            this.femaleRadioButton.TabIndex = 10;
            this.femaleRadioButton.TabStop = true;
            this.femaleRadioButton.Text = "Female";
            this.femaleRadioButton.UseVisualStyleBackColor = true;
            // 
            // maleRadioButton
            // 
            this.maleRadioButton.AutoSize = true;
            this.maleRadioButton.Location = new System.Drawing.Point(147, 188);
            this.maleRadioButton.Name = "maleRadioButton";
            this.maleRadioButton.Size = new System.Drawing.Size(48, 17);
            this.maleRadioButton.TabIndex = 11;
            this.maleRadioButton.TabStop = true;
            this.maleRadioButton.Text = "Male";
            this.maleRadioButton.UseVisualStyleBackColor = true;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(37, 225);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 12;
            this.calculateButton.Text = "Calculate Body Fat";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click_1);
            // 
            // bodyFatLabel
            // 
            this.bodyFatLabel.AutoSize = true;
            this.bodyFatLabel.Location = new System.Drawing.Point(44, 280);
            this.bodyFatLabel.Name = "bodyFatLabel";
            this.bodyFatLabel.Size = new System.Drawing.Size(107, 13);
            this.bodyFatLabel.TabIndex = 13;
            this.bodyFatLabel.Text = "Body Fat Percentage";
            // 
            // weightUnitLabel
            // 
            this.weightUnitLabel.AutoSize = true;
            this.weightUnitLabel.Location = new System.Drawing.Point(298, 19);
            this.weightUnitLabel.Name = "weightUnitLabel";
            this.weightUnitLabel.Size = new System.Drawing.Size(42, 13);
            this.weightUnitLabel.TabIndex = 15;
            this.weightUnitLabel.Text = "pounds";
            // 
            // waistUnitLabel
            // 
            this.waistUnitLabel.AutoSize = true;
            this.waistUnitLabel.Location = new System.Drawing.Point(298, 49);
            this.waistUnitLabel.Name = "waistUnitLabel";
            this.waistUnitLabel.Size = new System.Drawing.Size(85, 13);
            this.waistUnitLabel.TabIndex = 16;
            this.waistUnitLabel.Text = "inches (at navel)";
            // 
            // wristUnitLabel
            // 
            this.wristUnitLabel.AutoSize = true;
            this.wristUnitLabel.Location = new System.Drawing.Point(298, 89);
            this.wristUnitLabel.Name = "wristUnitLabel";
            this.wristUnitLabel.Size = new System.Drawing.Size(116, 13);
            this.wristUnitLabel.TabIndex = 17;
            this.wristUnitLabel.Text = "inches (at fullest point)*";
            // 
            // hipsUnitLabel
            // 
            this.hipsUnitLabel.AutoSize = true;
            this.hipsUnitLabel.Location = new System.Drawing.Point(298, 129);
            this.hipsUnitLabel.Name = "hipsUnitLabel";
            this.hipsUnitLabel.Size = new System.Drawing.Size(116, 13);
            this.hipsUnitLabel.TabIndex = 18;
            this.hipsUnitLabel.Text = "inches (at fullest point)*";
            // 
            // forearmUnitLabel
            // 
            this.forearmUnitLabel.AutoSize = true;
            this.forearmUnitLabel.Location = new System.Drawing.Point(298, 158);
            this.forearmUnitLabel.Name = "forearmUnitLabel";
            this.forearmUnitLabel.Size = new System.Drawing.Size(116, 13);
            this.forearmUnitLabel.TabIndex = 19;
            this.forearmUnitLabel.Text = "inches (at fullest point)*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "*Only required for female";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(115, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(201, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Essential Fat: Women 10-12%, Men 2-4%";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(115, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Athletes: Women 14-20%, Men 6-13%";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(115, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(186, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Fitness: Women 21-24%, Men 14-17%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(115, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(207, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Acceptable: Women 25-31%, Men 18-25%";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(115, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(166, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Obese: Women 32%+, Men 25%+";
            // 
            // BodyFatCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 459);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.tabControl1);
            this.Name = "BodyFatCalculator";
            this.Text = "Body Fat Calculator";
            this.tabControl1.ResumeLayout(false);
            this.calculatorPage.ResumeLayout(false);
            this.calculatorPage.PerformLayout();
            this.chartPage.ResumeLayout(false);
            this.chartPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage calculatorPage;
        private System.Windows.Forms.TabPage chartPage;
        private System.Windows.Forms.TextBox hipsTextBox;
        private System.Windows.Forms.TextBox wristTextBox;
        private System.Windows.Forms.TextBox waistTextBox;
        private System.Windows.Forms.TextBox weightTextBox;
        private System.Windows.Forms.Label hipsLabel;
        private System.Windows.Forms.Label wristLabel;
        private System.Windows.Forms.Label waistLabel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.TextBox forearmTextBox;
        private System.Windows.Forms.Label forearmLabel;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label bodyFatLabel;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.RadioButton maleRadioButton;
        private System.Windows.Forms.RadioButton femaleRadioButton;
        private System.Windows.Forms.Label forearmUnitLabel;
        private System.Windows.Forms.Label hipsUnitLabel;
        private System.Windows.Forms.Label wristUnitLabel;
        private System.Windows.Forms.Label waistUnitLabel;
        private System.Windows.Forms.Label weightUnitLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}

