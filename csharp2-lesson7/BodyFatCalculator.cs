﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace csharp2_lesson7
{
    public partial class BodyFatCalculator : Form
    {
        private bool _enableDebugging = true;
        public BodyFatCalculator()
        {
            InitializeComponent();
            femaleRadioButton.Checked = true;

        }

 
        
        private double calculateBodyFat(bool isFemale, double weightFactor, double wristFactor, 
                double waistFactor, double hipsFactor, double forearmFactor)
        {
            double leanBodyMass = 0;
            double bodyFatWeight = 0;
            double bodyWeight = weightFactor;

            if (isFemale)
            {
                // Calculate for female.
                weightFactor = weightFactor * 0.732 + 8.987;
                wristFactor /= 3.140;
                waistFactor *= 0.157;
                hipsFactor *= 0.249;
                forearmFactor *= 0.434;
                leanBodyMass = weightFactor + wristFactor - waistFactor - hipsFactor + forearmFactor;
                bodyFatWeight = bodyWeight - leanBodyMass;
            }
            else
            {
                // Calculate for male.
                weightFactor = weightFactor * 1.082 + 94.42;
                waistFactor *= 4.15;
                leanBodyMass = weightFactor - waistFactor;
                bodyFatWeight = bodyWeight - leanBodyMass;
            }

            return bodyFatWeight;

        }
        
        private void setToolTip(Control labelControl, Control textBoxControl, Control labelUnitControl, string toolText)
        {
            // Create a tooltip object.
            ToolTip tp = new ToolTip();
            
            // Set tooltip for each control.
            tp.SetToolTip(labelControl, toolText);
            tp.SetToolTip(textBoxControl, toolText);
            tp.SetToolTip(labelUnitControl, toolText);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void calculateButton_Click_1(object sender, EventArgs e)
        {
            writeToLogDebug("This is debugging information");
            double weightFactor = 0;
            double waistFactor = 0;
            double wristFactor = 0;
            double hipsFactor = 0;
            double forearmFactor = 0;
            double bodyFatPercentage = 0;
            bool isFemale = false;

            // See if female.
            if (femaleRadioButton.Checked)
                isFemale = true;

            // Set all unit labels to black.
            weightUnitLabel.ForeColor = Color.Black;
            waistUnitLabel.ForeColor = Color.Black;
            wristUnitLabel.ForeColor = Color.Black;
            hipsUnitLabel.ForeColor = Color.Black;
            forearmUnitLabel.ForeColor = Color.Black;

            // Attempt to convert TextBox values to doubles, marking unit labels red for failures.
            if (!double.TryParse(weightTextBox.Text, out weightFactor))
                weightUnitLabel.ForeColor = Color.Red;
            if (!double.TryParse(waistTextBox.Text, out waistFactor))
                waistUnitLabel.ForeColor = Color.Red;
            if (isFemale)
            {
                if (!double.TryParse(wristTextBox.Text, out wristFactor))
                    wristUnitLabel.ForeColor = Color.Red;
                if (!double.TryParse(hipsTextBox.Text, out hipsFactor))
                    hipsUnitLabel.ForeColor = Color.Red;
                if (!double.TryParse(forearmTextBox.Text, out forearmFactor))
                    forearmUnitLabel.ForeColor = Color.Red;
            }
            writeToLogDebug(weightUnitLabel.ForeColor.ToString());
            writeToLogDebug(waistUnitLabel.ForeColor.ToString());
            writeToLogDebug(wristUnitLabel.ForeColor.ToString());
            writeToLogDebug(hipsUnitLabel.ForeColor.ToString());
            writeToLogDebug(forearmUnitLabel.ForeColor.ToString());
            // Make sure all values are above zero.
            if (weightFactor <= 0)
                weightUnitLabel.ForeColor = Color.Red;
            if (waistFactor <= 0)
                waistUnitLabel.ForeColor = Color.Red;
            if (isFemale)
            {
                if (wristFactor <= 0)
                    wristUnitLabel.ForeColor = Color.Red;
                if (hipsFactor <= 0)
                    hipsUnitLabel.ForeColor = Color.Red;
                if (forearmFactor <= 0)
                    forearmUnitLabel.ForeColor = Color.Red;
            }

            // See if there were any errors by checking Unit label Forecolor.
            // Weight
            if (weightUnitLabel.ForeColor == Color.Red)
                setToolTip(weightLabel, weightTextBox, weightUnitLabel, "Weight (pounds) must be numeric and above 0.");
            else
                setToolTip(weightLabel, weightTextBox, weightUnitLabel, "");
            // Waist
            if (waistUnitLabel.ForeColor == Color.Red)
                setToolTip(waistLabel, waistTextBox, waistUnitLabel, "Waist (inches) must be numeric and above 0.");
            else
                setToolTip(waistLabel, waistTextBox, waistUnitLabel, "");
            if (isFemale)
            {
                // Wrist
                if (wristUnitLabel.ForeColor == Color.Red)
                    setToolTip(wristLabel, wristTextBox, wristUnitLabel, "Wrist (inches) must be numeric and above 0.");
                else
                    setToolTip(wristLabel, wristTextBox, wristUnitLabel, "");
                // Hips
                if (hipsUnitLabel.ForeColor == Color.Red)
                    setToolTip(hipsLabel, hipsTextBox, hipsUnitLabel, "Hips (inches) must be numeric and above 0.");
                else
                    setToolTip(hipsLabel, hipsTextBox, hipsUnitLabel, "");
                // Forearm
                if (forearmUnitLabel.ForeColor == Color.Red)
                    setToolTip(forearmLabel, forearmTextBox, forearmUnitLabel, "Forearm (inches) must be numeric and above 0.");
                else
                    setToolTip(forearmLabel, forearmTextBox, forearmUnitLabel, "");
            }
            writeToLogDebug(weightLabel.Text);
            writeToLogDebug(weightTextBox.Text);
            writeToLogDebug(weightUnitLabel.Text);
            writeToLogDebug(waistLabel.Text);
            writeToLogDebug(waistTextBox.Text);
            writeToLogDebug(waistUnitLabel.Text);
            writeToLogDebug(wristLabel.Text);
            writeToLogDebug(wristTextBox.Text);
            writeToLogDebug(wristUnitLabel.Text);

            writeToLogDebug(hipsLabel.Text);
            writeToLogDebug(hipsTextBox.Text);
            writeToLogDebug(hipsUnitLabel.Text);


            bool isError = false;

            if (!isError)
            {
                // Display the body fat Label.
                bodyFatLabel.Visible = true;
                bodyFatLabel.ForeColor = Color.Red;

                // No errors, so calculate and display body fat percentage, formatting to two decimal places.
                bodyFatPercentage = (calculateBodyFat(isFemale, weightFactor, wristFactor, waistFactor, hipsFactor, forearmFactor) * 100) / weightFactor;
                bodyFatLabel.Text = "Body Fat Percentage: " + bodyFatPercentage.ToString("#0.00") + "%";
            }
            else
            {
                // There were errors.
                bodyFatLabel.Visible = true;
                bodyFatLabel.ForeColor = Color.Red;
                bodyFatLabel.Text = "Hover mouse over error to see error message";
            }
           
            writeToLogDebug(bodyFatLabel.Text);
            writeToLogDebug(bodyFatLabel.Visible.ToString());
            writeToLogDebug(bodyFatLabel.ForeColor.ToString());
            writeToLogDebug(bodyFatPercentage.ToString());
            writeToLog(bodyFatLabel.Text);
            
        }

        private void writeToLog(string ans)
        {
            using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory()+"\\log.txt"))
            {
                sw.WriteLine(DateTime.Now.ToString()+" "+ans+"\n");
                sw.Close();
            }
            
        }

        private void writeToLogDebug(string s)
        {
            using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\log.txt"))
            {
                sw.WriteLine("This is debugging information.");
                sw.WriteLine(DateTime.Now.ToString() + " " + s + "\n");
                sw.Close();
            }
        }
    }
}

